<?php

/*
Plugin Name: Just Breadcrumbs
Plugin URI: https://planetjon.ca/projects/just-breadcrumbs/
Description: For generating a breadcrumb trail
Version: 1.0
Requires at least: 3.5.0
Tested up to: 5.0
Requires PHP: 5.4
Author: Jonathan Weatherhead
Author URI: https://planetjon.ca
License: GPL2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

namespace planetjon\just_breadcrumbs;

const title_key = 0;
const href_key = 1;

function breadcrumbs() {
    $breadcrumbs = [
        [
            get_bloginfo( 'name' ),
            get_home_url()
        ]
    ];

    if( is_single() ) {
        if( 'page' === get_option( 'show_on_front' ) ) {
            $breadcrumbs []= [
                __( 'blog', 'just-breadcrumbs' ),
                get_permalink( get_option( 'page_for_posts' ) )
            ];
        }

        $breadcrumbs []= [
            get_the_title( get_queried_object() )
        ];
    }
    elseif( is_front_page() || is_search() || is_404() ) {
        $breadcrumbs = [];
    }
    elseif( is_page() ) {
        $ancestors = array_reverse( get_ancestors( get_queried_object_id(), '', 'post_type' ) );
        foreach( $ancestors as $ancestor ) {
            $breadcrumbs []= [
                get_the_title( $ancestor ),
                get_permalink( $ancestor )
            ];
        }

        $breadcrumbs []= [
            get_the_title( get_queried_object() ),
        ];
    }
    elseif( is_category() || is_tag() || is_tax() ) {
        $ancestors = array_reverse( get_ancestors( get_queried_object_id(), '', 'taxonomy' ) );
        foreach( $ancestors as $ancestor ) {
            $breadcrumbs []= [
                get_term( $ancestor )->name,
                get_term_link( $ancestor )
            ];
        }

        $breadcrumbs []= [
            single_term_title( '', false )
        ];
    }
    elseif( is_home() && !is_front_page() ){
        $breadcrumbs []= [
            __( 'blog', 'just-breadcrumbs' )
        ];
    }
    elseif( is_author() ) {
        $breadcrumbs []= [
            get_the_author_meta( 'display_name', get_queried_object_id() )
        ];
    }
    elseif( is_post_type_archive() ) {
        $breadcrumbs []= [
            post_type_archive_title( '', false )
        ];
    }

    $breadcrumblength = count( $breadcrumbs );
    echo '<nav aria-label="Breadcrumb" class="just-breadcrumbs-container"><ul>';
    foreach( $breadcrumbs as $idx => $breadcrumb ) {
        $lastcrumb = $idx + 1 === $breadcrumblength;

        $classes = ['breadcrumb'];
        $aria = [];
        if( $lastcrumb ) {
            $classes []= 'last-breadcrumb';
            $aria []= 'aria-current="page"';
        }

        echo '<li>';
        if( !empty( $breadcrumb[href_key] ) ) {
            $classes []= 'has-href';
            printf(
                '<a %s class="%s" href="%s">%s</a>',
                join( ' ', $aria ),
                join( ' ', $classes ),
                $breadcrumb[href_key],
                $breadcrumb[title_key]
            );
        }
        else {
            $classes []= 'no-href';
            printf(
                '<span %s class="%s">%s</span>',
                join( ' ', $aria ),
                join( ' ', $classes ),
                $breadcrumb[title_key]
            );
        }
        echo '</li>';
    }
    echo '</ul></nav>';
}